import { GraphQLError, Kind } from 'graphql';
import { schemaComposer } from '../graphql-compose';

/* eslint-disable no-useless-escape */
const EMAIL_ADDRESS_REGEX = new RegExp(
  /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
);
/* eslint-enable */

export const EmailAddressSTC = schemaComposer.createScalarTC({
  name: 'EmailAddress',

  description:
    'A field whose value conforms to the standard internet email address format as specified in RFC822: https://www.w3.org/Protocols/rfc822/.',

  serialize(value: any) {
    if (typeof value !== 'string') {
      throw new TypeError(`Value is not string: ${value}`);
    }

    const normalizedEmail = value.toLowerCase().trim();

    if (!EMAIL_ADDRESS_REGEX.test(normalizedEmail)) {
      throw new TypeError(`Value is not a valid email address: ${value}`);
    }

    return normalizedEmail;
  },

  parseValue(value: any) {
    if (typeof value !== 'string') {
      throw new TypeError('Value is not string');
    }

    const normalizedEmail = value.toLowerCase().trim();

    if (!EMAIL_ADDRESS_REGEX.test(normalizedEmail)) {
      throw new TypeError(`Value is not a valid email address: ${value}`);
    }

    return normalizedEmail;
  },

  parseLiteral(ast: any) {
    if (ast.kind !== Kind.STRING) {
      throw new GraphQLError(`Can only validate strings as email addresses but got a: ${ast.kind}`);
    }

    const normalizedEmail = ast.value.toLowerCase().trim();

    if (!EMAIL_ADDRESS_REGEX.test(normalizedEmail)) {
      throw new TypeError(`Value is not a valid email address: ${ast.value}`);
    }

    return normalizedEmail;
  },
});
