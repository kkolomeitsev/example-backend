import express from 'express';
import { FieldNode } from 'graphql';
import DataLoader from 'dataloader';

export interface TContext {
  dataloaders: WeakMap<ReadonlyArray<FieldNode>, DataLoader<string, object>>;
  req: express.Request;
  res: express.Response;
  params: any;
}

export async function prepareContext({
  req,
  res,
  ...params
}: {
  req: express.Request;
  res: express.Response;
}): Promise<TContext> {
  return {
    dataloaders: new WeakMap(),
    req,
    res,
    params,
  };
}

export async function dropSession(context: TContext) {
  if (context.req.user) {
    context.req.logout();
    const sessionRegenerate = new Promise((resolve) => {
      if (context.req.session) {
        context.req.session.regenerate((err: any) => {
          if (err) {
            console.log('Regenerate destroy error!', err);
          }
          return resolve(true);
        });
      } else {
        return resolve(true);
      }
    });
    await sessionRegenerate;
    if (context.req.session) {
      if (context.req.session.userId) context.req.session.userId = undefined;
    }
  }
}
