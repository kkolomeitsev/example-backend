import { FieldConfig } from 'app/schema/graphql-compose';
import { withFilter } from 'apollo-server-express';
import pubSub from 'app/utils/pubSub';

const myQueue: FieldConfig<{}> = {
  type: `type MyQueueSubscription { upd: Boolean, msg: String }`,
  args: {
    queue: 'String',
  },
  subscribe: withFilter(
    () => pubSub.asyncIterator(['myQueueChanged']),
    (source, args) => {
      // const _sessionData = context?.params?.connection?.context?.req?.session;
      return source.queue === args.queue;
    }
  ),
};

export default myQueue;
