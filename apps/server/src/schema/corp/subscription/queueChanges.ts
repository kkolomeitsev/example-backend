import { FieldConfig } from 'app/schema/graphql-compose';
import { withFilter } from 'apollo-server-express';
import pubSub from 'app/utils/pubSub';

const queueChanges: FieldConfig<{}> = {
  type: `type QueueChangesSubscription { upd: Boolean, msg: String, newTurn: Int }`,
  args: {
    queue: 'String',
  },
  subscribe: withFilter(
    () => pubSub.asyncIterator(['QueueChanged']),
    (source, args) => {
      // const _sessionData = context?.params?.connection?.context?.req?.session;
      return source.queue === args.queue;
    }
  ),
};

export default queueChanges;
