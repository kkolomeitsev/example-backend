import { FieldConfig } from 'app/schema/graphql-compose';
import { UserDocument } from 'app/models/User';
import { Queue, QueueStatus } from 'app/models/Queue';
import { HttpError } from 'app/utils/errors';
import { closeCurrentEntry } from './changeQueueStatus';
import { Entry } from 'app/models/Entry';
import pubSub from 'app/utils/pubSub';

const changeQueueCurrentEntry: FieldConfig<{
  stopCurrent: boolean;
  inviteNext: boolean;
  currentNotCome: boolean;
}> = {
  type: 'Boolean',
  args: {
    stopCurrent: 'Boolean',
    inviteNext: 'Boolean',
    currentNotCome: 'Boolean',
  },
  resolve: async (_, args, ctx) => {
    const user: UserDocument | undefined = ctx.req.user as any;
    if (!user) {
      throw new HttpError(401, 'Not authorized');
    }
    if (!user.queue) {
      throw new HttpError(403, 'User do not have the queue');
    }
    const queue = await Queue.findById(user.queue).exec();
    if (!queue) {
      throw new HttpError(403, 'User do not have the queue');
    }

    if (queue.status !== QueueStatus.OPEN) {
      throw new HttpError(403, 'Until the queue is open, you cannot invite entry');
    }

    const { stopCurrent, inviteNext, currentNotCome } = args;

    if (!stopCurrent && !inviteNext && !currentNotCome) {
      return true;
    }

    if (currentNotCome) {
      if (queue.currentEntry) {
        const currentEntry = await Entry.findById(queue.currentEntry).exec();
        if (currentEntry) {
          currentEntry.inLife = false;
          await currentEntry.save();
        }
      }
      queue.currentEntry = undefined;
    } else {
      await closeCurrentEntry(queue);
    }

    let newTurn: number | undefined;

    if (inviteNext && queue.entries && queue.entries.length) {
      await queue
        .populate({ path: 'entries', options: { sort: 'commonTurn', limit: 1 } })
        .execPopulate();

      const currentEntry = (queue.entries as any[])?.[0];

      newTurn = currentEntry?.commonTurn;

      queue.currentEntry = currentEntry?._id || undefined;

      queue.depopulate('entries');

      const queueEntriesIndex = queue.entries
        ? queue.entries.findIndex((el) => String(el) === String(queue.currentEntry))
        : -1;

      if (queueEntriesIndex !== -1) {
        queue.entries?.splice(queueEntriesIndex, 1);
      }
    }

    await queue.save();

    pubSub.publish('QueueChanged', {
      queue: queue.link,
      queueChanges: {
        upd: true,
        msg: `Изменение в очереди`,
        newTurn,
      },
    });

    return true;
  },
};

export default changeQueueCurrentEntry;
