import { FieldConfig } from 'app/schema/graphql-compose';
import { UserModel, UserDocument } from 'app/models/User';
import { QueryUserTC, UpdateUserITC } from 'app/schema/types';
import { HttpError } from 'app/utils/errors';
import { escapeRegExp, escapePhoneRegExp } from 'app/utils/escapeRegExp';
import { sanitizeMessage } from 'app/utils/sanitizeMessage';

const updateProfile: FieldConfig<{ input: UserModel }> = {
  type: QueryUserTC,
  args: {
    input: UpdateUserITC.getTypeNonNull(),
  },
  resolve: async (_, args, ctx) => {
    const user: UserDocument | undefined = ctx.req.user as any;
    if (!user) {
      throw new HttpError(401, 'Not authorized');
    }
    const cleanInput: UserModel = JSON.parse(JSON.stringify(args.input));
    if (cleanInput['title']) {
      cleanInput['title'] = escapeRegExp(sanitizeMessage(cleanInput['title'] || ''));
    }
    if (cleanInput['phone']) {
      cleanInput['phone'] = escapePhoneRegExp(sanitizeMessage(cleanInput['phone'] || ''));
    }
    const result = await user.updateData(cleanInput);
    return result;
  },
};

export default updateProfile;
