import { FieldConfig } from 'app/schema/graphql-compose';
import { UserDocument } from 'app/models/User';
import { QueueModel, Queue } from 'app/models/Queue';
import { QueryQueueTC, QueueITC } from 'app/schema/types';
import { HttpError } from 'app/utils/errors';
import { escapeRegExp } from 'app/utils/escapeRegExp';
import { sanitizeMessage } from 'app/utils/sanitizeMessage';
import { getUniqueRandomString } from 'app/utils/randomString';
import moment from 'moment';

const createQueue: FieldConfig<{ input: QueueModel }> = {
  type: QueryQueueTC,
  args: {
    input: QueueITC.getTypeNonNull(),
  },
  resolve: async (_, args, context) => {
    const user: UserDocument | undefined = context.req.user as any;
    if (!user) {
      throw new HttpError(401, 'Not authorized');
    }
    if (user.queue) {
      throw new HttpError(403, 'Already has queue');
    }
    const cleanInput: QueueModel = JSON.parse(JSON.stringify(args.input));

    if (cleanInput['recurringWeekend']) {
      const reqW = Array.isArray(cleanInput['recurringWeekend'])
        ? cleanInput['recurringWeekend']
        : [];
      if (reqW.length > 7) {
        throw new HttpError(403, 'You cant add more than 7 reccurent weekends');
      }
      reqW.forEach((el) => {
        if (!['1', '2', '3', '4', '5', '6', '7'].includes(el)) {
          throw new HttpError(403, 'Wrong day of week');
        }
      });
      cleanInput['recurringWeekend'] = reqW;
    }

    if (cleanInput['holidays']) {
      const holidays = Array.isArray(cleanInput['holidays']) ? cleanInput['holidays'] : [];
      const cleanHolidays: string[] = [];
      let error = false;
      const currentDate = moment()
        .utcOffset(cleanInput['gmt'] || 6)
        .startOf('day');

      holidays.forEach((el) => {
        try {
          const hDate = moment(el);
          if (!hDate.isValid()) {
            error = true;
          } else {
            const cleanDate = hDate
              .clone()
              .utcOffset(cleanInput['gmt'] || 6)
              .startOf('day');
            if (cleanDate.isSameOrAfter(currentDate)) {
              cleanHolidays.push(cleanDate.toISOString());
            }
          }
        } catch (_e) {
          error = true;
        }
      });

      if (error) {
        throw new HttpError(403, 'Wrong holiday day format');
      }

      cleanInput['holidays'] = cleanHolidays;
    }

    cleanInput['owner'] = user._id;
    cleanInput['title'] = escapeRegExp(sanitizeMessage(cleanInput['title'] || ''));
    cleanInput['description'] = sanitizeMessage(cleanInput['description'] || '');
    cleanInput['breakMessage'] = sanitizeMessage(cleanInput['breakMessage'] || '');
    cleanInput['link'] = await getUniqueRandomString(Queue, 'link');
    const newQueue = await new Queue(cleanInput).save();
    user.queue = newQueue._id;
    await user.save();
    return newQueue;
  },
};

export default createQueue;
