import { FieldConfig } from 'app/schema/graphql-compose';
import { UserDocument, User } from 'app/models/User';
import { Queue, QueueStatus, QueueDocument } from 'app/models/Queue';
import { QueryQueueTC, QueueStatusChangeITC } from 'app/schema/types';
import { HttpError } from 'app/utils/errors';
import { sanitizeMessage } from 'app/utils/sanitizeMessage';
import { Entry } from 'app/models/Entry';
import pubSub from 'app/utils/pubSub';

export async function closeCurrentEntry(queue: QueueDocument, withSave = false) {
  if (queue.currentEntry) {
    const currentEntry = await Entry.findById(queue.currentEntry).exec();
    if (currentEntry) {
      currentEntry.inLife = false;
      currentEntry.active = false;
      await currentEntry.save();

      const currentEntryUser = await User.findById(currentEntry.user).exec();
      if (currentEntryUser) {
        const currentEntryUserIndex = currentEntryUser.entries
          ? currentEntryUser.entries.findIndex((el) => String(el) === String(currentEntry._id))
          : -1;

        if (currentEntryUserIndex !== -1) {
          currentEntryUser.entries?.splice(currentEntryUserIndex, 1);
          await currentEntryUser.save();
        }
      }
    }

    queue.currentEntry = undefined;

    if (withSave) {
      await queue.save();
    }
  }
}

interface UpdateStatusInput {
  status?: QueueStatus;
  breakMessage?: string;
  breakEntry: { _id: string };
}

const changeQueueStatus: FieldConfig<{
  input: UpdateStatusInput;
}> = {
  type: QueryQueueTC,
  args: {
    input: QueueStatusChangeITC.getTypeNonNull(),
  },
  resolve: async (_, args, ctx) => {
    const user: UserDocument | undefined = ctx.req.user as any;
    if (!user) {
      throw new HttpError(401, 'Not authorized');
    }
    if (!user.queue) {
      throw new HttpError(403, 'User do not have the queue');
    }
    const queue = await Queue.findById(user.queue).exec();
    if (!queue) {
      throw new HttpError(403, 'User do not have the queue');
    }

    const cleanInput: UpdateStatusInput = JSON.parse(JSON.stringify(args.input));

    if (cleanInput.breakMessage) {
      cleanInput['breakMessage'] = sanitizeMessage(cleanInput['breakMessage'] || '');
    }

    if (cleanInput.breakEntry) {
      const breakEntry = await Entry.findOne({
        _id: cleanInput.breakEntry._id,
        queue: queue._id,
        active: true,
        inLife: true,
      }).exec();

      if (!breakEntry) {
        throw new HttpError(403, 'You can put a break only on a recording from a live queue!');
      }

      cleanInput['breakEntry'] = breakEntry._id;
    }

    if (cleanInput.status) {
      if (cleanInput.status === QueueStatus.BREAK) {
        if (queue.status === QueueStatus.CLOSED) {
          throw new HttpError(403, 'You cannot take a break without opening the queue!');
        }

        await closeCurrentEntry(queue);
      } else if (cleanInput.status === QueueStatus.OPEN) {
        if (queue.status === QueueStatus.CLOSED) {
          // Уведомить об открытии очереди
        }
      } else if (cleanInput.status === QueueStatus.CLOSED) {
        if (queue.status === QueueStatus.BREAK) {
          throw new HttpError(403, 'You cannot close the queue at once after break!');
        }

        await closeCurrentEntry(queue);

        const queueEntries: any = { _id: { $in: queue?.entries || [] } };

        await Entry.updateMany(queueEntries, { inLife: false }).exec();

        queue.entries = [];
        queue.breakEntry = undefined;

        if (cleanInput['breakEntry']) {
          delete cleanInput['breakEntry'];
        }
      }

      let title = '';

      switch (cleanInput.status.trim().toLowerCase()) {
        case 'open':
          title = 'Открыто';
          break;
        case 'closed':
          title = 'Закрыто';
          break;
        case 'break':
          title = 'Перерыв';
          break;
      }

      pubSub.publish('QueueChanged', {
        queue: queue.link,
        queueChanges: {
          upd: true,
          msg: `Статус очереди изменился на: ${title}`,
        },
      });
    }

    Object.keys(cleanInput).forEach((el) => {
      queue[el] = cleanInput[el];
    });

    await queue.save();

    return queue;
  },
};

export default changeQueueStatus;
