import moment from 'moment';
import { FieldConfig } from 'app/schema/graphql-compose';
import { UserDocument } from 'app/models/User';
import { Queue } from 'app/models/Queue';
import { Entry } from 'app/models/Entry';
import { HttpError } from 'app/utils/errors';
import pubSub from 'app/utils/pubSub';

const cancelEntry: FieldConfig<{ entryId: string }> = {
  type: 'Boolean',
  args: {
    entryId: 'MongoID!',
  },
  resolve: async (_, args, context) => {
    const user: UserDocument | undefined = context.req.user as any;
    if (!user) {
      throw new HttpError(401, 'Not authorized');
    }

    const entry = await Entry.findOne({ _id: args.entryId, user: user._id, active: true }).exec();

    if (!entry) {
      throw new HttpError(403, 'Entry not found!');
    }

    const inLife = entry.inLife;

    entry.inLife = false;
    entry.active = false;
    await entry.save();

    const userEntriesIndex = user.entries
      ? user.entries.findIndex((el) => String(el) === String(entry._id))
      : -1;

    if (userEntriesIndex !== -1) {
      user.entries?.splice(userEntriesIndex, 1);
      await user.save();
    }

    const queue = await Queue.findById(entry.queue).exec();
    if (!queue) {
      throw new HttpError(403, 'Queue not found');
    }

    const currentDate = moment().utcOffset(queue.gmt);
    const entryDate = moment(entry.date).utcOffset(queue.gmt);

    const daysDiff = entryDate
      .clone()
      .startOf('day')
      .diff(currentDate.clone().startOf('day'), 'days');

    if (daysDiff === 0) {
      pubSub.publish('myQueueChanged', {
        queue: queue.link,
        myQueue: {
          upd: true,
          msg: 'Человек отменил запись',
        },
      });
    }

    if (inLife) {
      const queueEntriesIndex = queue.entries
        ? queue.entries.findIndex((el) => String(el) === String(entry._id))
        : -1;

      let needToSave = false;

      if (queueEntriesIndex !== -1) {
        queue.entries?.splice(queueEntriesIndex, 1);
        needToSave = true;
      }

      if (String(queue.breakEntry) === String(entry._id)) {
        if (queue.entries?.length) {
          if (queue.entries?.[queueEntriesIndex]) {
            queue.breakEntry = queue.entries[queueEntriesIndex] || undefined;
          } else {
            queue.breakEntry = queue.entries?.[queueEntriesIndex - 1] || undefined;
          }
        } else {
          queue.breakEntry = undefined;
        }
        needToSave = true;
      }

      if (String(queue.currentEntry) === String(entry._id)) {
        queue.currentEntry = undefined;
        needToSave = true;
      }

      if (needToSave) {
        await queue.save();
      }

      if (daysDiff === 0) {
        pubSub.publish('QueueChanged', {
          queue: queue.link,
          queueChanges: {
            upd: true,
            msg: 'Кто-то вышел из очереди',
          },
        });
      }
    }

    return true;
  },
};

export default cancelEntry;
