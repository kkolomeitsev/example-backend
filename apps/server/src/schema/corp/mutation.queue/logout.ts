import { FieldConfig } from 'app/schema/graphql-compose';
import { dropSession } from 'app/schema/corp/context';

const logout: FieldConfig<{ username: string; password: string; remember: boolean }> = {
  type: 'Boolean',
  resolve: async (_, __, ctx) => {
    if (!ctx.req.user) {
      throw new Error('Already logout');
    }
    await dropSession(ctx);
    return true;
  },
};

export default logout;
