import { FieldConfig } from 'app/schema/graphql-compose';
import { UserDocument } from 'app/models/User';
import { Queue, QueueStatus } from 'app/models/Queue';
import { EntryModel, Entry, EntryDocument } from 'app/models/Entry';
import { QueryEntryTC, EntryITC } from 'app/schema/types';
import { HttpError } from 'app/utils/errors';
import { sanitizeMessage } from 'app/utils/sanitizeMessage';
import { checkQueuePaid } from 'app/utils/checkQueuePaid';
import pubSub from 'app/utils/pubSub';
import moment, { Moment } from 'moment';

function compareTime(timeFrom: string, timeTo: string, bookingDate: Moment): boolean {
  const timeFromMoment = moment(timeFrom, 'HH:mm:ss.SSS');
  const timeToMoment = moment(timeTo, 'HH:mm:ss.SSS');
  const bookingDateTimeFrom = bookingDate
    .clone()
    .set('hour', timeFromMoment.hours())
    .set('minute', timeFromMoment.minutes())
    .set('second', timeFromMoment.seconds())
    .set('millisecond', timeFromMoment.milliseconds());
  const bookingDateTimeTo = bookingDate
    .clone()
    .set('hour', timeToMoment.hours())
    .set('minute', timeToMoment.minutes())
    .set('second', timeToMoment.seconds())
    .set('millisecond', timeToMoment.milliseconds());

  const result = bookingDate.isBetween(bookingDateTimeFrom, bookingDateTimeTo, undefined, '[)');
  return result;
}

const createEntry: FieldConfig<{ input: EntryModel }> = {
  type: QueryEntryTC,
  args: {
    input: EntryITC.getTypeNonNull(),
  },
  resolve: async (_, args, context) => {
    const user: UserDocument | undefined = context.req.user as any;
    if (!user) {
      throw new HttpError(401, 'Not authorized');
    }

    const queue = await Queue.findOne(args.input.queue).exec();
    if (!queue || !checkQueuePaid(queue)) {
      throw new HttpError(403, 'Queue not found');
    }

    const currentDate = moment().utcOffset(queue.gmt);
    const entryDate = moment(args.input.date).utcOffset(queue.gmt);

    if (entryDate.clone().add(10, 'minutes').isBefore(currentDate)) {
      throw new Error('You are trying to schedule on past date');
    }

    const daysDiff = entryDate
      .clone()
      .startOf('day')
      .diff(currentDate.clone().startOf('day'), 'days');

    if (daysDiff > queue.bookingPeriodDays) {
      throw new Error('In this queue you can’t sign up so far to the future');
    }

    if (args.input.inLife && (queue.status === QueueStatus.CLOSED || daysDiff > 0)) {
      throw new HttpError(403, "Queue is closed, you can't create in live entry");
    }

    if (
      queue.recurringWeekend.includes(String(entryDate.clone().isoWeekday())) &&
      !args.input.inLife
    ) {
      throw new HttpError(403, "Queue is closed, it's a day off");
    }

    const inOpenHours = compareTime(queue.workingHours[0], queue.workingHours[1], entryDate);

    if (!inOpenHours && !args.input.inLife) {
      throw new Error('You are trying to schedule on non-working hours');
    }

    const dateFrom = entryDate.clone().startOf('day');
    const dateTo = dateFrom.clone().endOf('day');

    const requests: Promise<EntryDocument | null>[] = [];

    requests[0] = Entry.findOne({
      queue: queue._id,
      date: { $gte: dateFrom.toDate(), $lte: dateTo.toDate() },
      active: true,
    })
      .sort('-commonTurn')
      .exec();

    requests[1] = Entry.findOne({
      queue: queue._id,
      user: user._id,
      date: { $gte: dateFrom.toDate(), $lte: dateTo.toDate() },
      active: true,
    }).exec();

    requests[2] = Entry.count({
      queue: queue._id,
      user: user._id,
      date: { $gte: dateFrom.toDate(), $lte: dateTo.toDate() },
    }).exec() as any;

    requests[3] = Entry.count({
      queue: queue._id,
      user: user._id,
      date: { $gte: dateFrom.toDate() },
    }).exec() as any;

    const results = await Promise.all(requests);

    const queueLastEntry = results[0];
    const checkMeEntry = results[1];
    const checkDayLimit: number = results[2] as any;
    const checkMaxLimit: number = results[3] as any;

    if (checkMeEntry) {
      throw new HttpError(403, 'You already has entry in this queue on this day');
    }

    if (
      queue.dayLimit !== 0 &&
      checkDayLimit >= queue.dayLimit &&
      (queue.limitLifeQueue || !args.input.inLife)
    ) {
      throw new HttpError(403, 'For this queue day limit is full');
    }

    if (
      queue.commonLimit !== 0 &&
      checkMaxLimit >= queue.commonLimit &&
      (queue.limitLifeQueue || !args.input.inLife)
    ) {
      throw new HttpError(403, 'For this queue limit is full');
    }

    const cleanInput: EntryModel = JSON.parse(JSON.stringify(args.input));
    cleanInput['date'] = entryDate.toDate();
    cleanInput['user'] = user._id;
    cleanInput['queue'] = queue._id;
    cleanInput['commonTurn'] = (queueLastEntry?.commonTurn || 0) + 1;
    cleanInput['description'] = sanitizeMessage(cleanInput['description'] || '');

    const newEntry = await new Entry(cleanInput).save();

    if (Array.isArray(user.entries)) {
      user.entries.push(newEntry._id);
    } else {
      user.entries = [newEntry._id];
    }
    await user.save();

    if (newEntry.inLife) {
      if (Array.isArray(queue.entries)) {
        queue.entries.push(newEntry._id);
      } else {
        queue.entries = [newEntry._id];
      }
    }
    await queue.save();

    if (daysDiff === 0) {
      pubSub.publish('myQueueChanged', {
        queue: queue.link,
        myQueue: {
          upd: true,
          msg: `Новый человек в${newEntry.inLife ? ' живой' : ''} очереди`,
        },
      });

      if (newEntry.inLife) {
        pubSub.publish('QueueChanged', {
          queue: queue.link,
          queueChanges: {
            upd: true,
            msg: 'Новый человек в живой очереди',
          },
        });
      }
    }

    return newEntry;
  },
};

export default createEntry;
