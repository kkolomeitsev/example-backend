import { FieldConfig } from 'app/schema/graphql-compose';
import { dropSession } from 'app/schema/corp/context';
import { User, UserDocument } from 'app/models/User';
import { QueryUserTC } from 'app/schema/types';
import { getFieldsForPath } from 'app/utils/getRelationsForField';

const login: FieldConfig<{ username: string; password: string; remember: boolean }> = {
  type: QueryUserTC,
  args: {
    username: 'EmailAddress!',
    password: 'String!',
    remember: 'Boolean',
  },
  resolve: async (_, args, context, info) => {
    await dropSession(context);
    const argUsername: string = args.username;
    args.username = argUsername.toLowerCase().trim();
    const auth: any = await User.login({ args, context, source: null, info: null });
    if (!auth.authorize) {
      throw new Error('UNAUTHORIZED');
    }
    const user: UserDocument | undefined = context.req.user as any;
    const needQueue = getFieldsForPath(info).find((el) => el.startsWith('queue'));
    if (needQueue && user && user.queue) {
      await user.populate('queue').execPopulate();
    }
    return context.req.user;
  },
};

export default login;
