import { FieldConfig } from 'app/schema/graphql-compose';
import { UserDocument } from 'app/models/User';
import { Queue } from 'app/models/Queue';
import { QueryOrderTC, OrderITC } from 'app/schema/types';
import { HttpError } from 'app/utils/errors';
import { OrderModel, Order } from 'app/models/Order';

const createOrder: FieldConfig<{ input: OrderModel }> = {
  type: QueryOrderTC,
  args: {
    input: OrderITC.getTypeNonNull(),
  },
  resolve: async (_, args, context) => {
    const user: UserDocument | undefined = context.req.user as any;
    if (!user) {
      throw new HttpError(401, 'Not authorized');
    }
    if (!user.queue) {
      throw new HttpError(403, 'User do not have the queue');
    }
    const queue = await Queue.findById(user.queue).exec();
    if (!queue) {
      throw new HttpError(403, 'Queue not found');
    }

    const orderData = {
      user: user._id,
      queue: queue._id,
    };

    const order = await Order.findOne({
      ...orderData,
      status: 'pending',
    }).exec();

    if (order) {
      return order;
    }

    const newOrder = await new Order({ ...orderData, amount: '4990' }).save();

    return newOrder;
  },
};

export default createOrder;
