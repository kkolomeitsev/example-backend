import { FieldConfig } from 'app/schema/graphql-compose';
import { UserDocument } from 'app/models/User';
import { Queue, QueueModel } from 'app/models/Queue';
import { QueryQueueTC, UpdateQueueITC } from 'app/schema/types';
import { HttpError } from 'app/utils/errors';
import { escapeRegExp } from 'app/utils/escapeRegExp';
import { sanitizeMessage } from 'app/utils/sanitizeMessage';
import moment from 'moment';

const updateQueue: FieldConfig<{ input: QueueModel }> = {
  type: QueryQueueTC,
  args: {
    input: UpdateQueueITC.getTypeNonNull(),
  },
  resolve: async (_, args, ctx) => {
    const user: UserDocument | undefined = ctx.req.user as any;
    if (!user) {
      throw new HttpError(401, 'Not authorized');
    }
    if (!user.queue) {
      throw new HttpError(403, 'User do not have the queue');
    }
    const queue = await Queue.findById(user.queue).exec();
    if (!queue) {
      throw new HttpError(403, 'User do not have the queue');
    }

    const cleanInput: QueueModel = JSON.parse(JSON.stringify(args.input));

    if (cleanInput['recurringWeekend']) {
      const reqW = Array.isArray(cleanInput['recurringWeekend'])
        ? cleanInput['recurringWeekend']
        : [];
      if (reqW.length > 7) {
        throw new HttpError(403, 'You cant add more than 7 reccurent weekends');
      }
      reqW.forEach((el) => {
        if (!['1', '2', '3', '4', '5', '6', '7'].includes(el)) {
          throw new HttpError(403, 'Wrong day of week');
        }
      });
      cleanInput['recurringWeekend'] = reqW;
    }

    if (cleanInput['holidays']) {
      const holidays = Array.isArray(cleanInput['holidays']) ? cleanInput['holidays'] : [];
      const cleanHolidays: string[] = [];
      let error = false;
      const currentDate = moment()
        .utcOffset(cleanInput['gmt'] || 6)
        .startOf('day');

      holidays.forEach((el) => {
        try {
          const hDate = moment(el);
          if (!hDate.isValid()) {
            error = true;
          } else {
            const cleanDate = hDate
              .clone()
              .utcOffset(cleanInput['gmt'] || 6)
              .startOf('day');
            if (cleanDate.isSameOrAfter(currentDate)) {
              cleanHolidays.push(cleanDate.toISOString());
            }
          }
        } catch (_e) {
          error = true;
        }
      });

      if (error) {
        throw new HttpError(403, 'Wrong holiday day format');
      }

      cleanInput['holidays'] = cleanHolidays;
    }

    Object.keys(cleanInput).forEach((el) => {
      if (['title', 'description', 'breakMessage'].includes(el)) {
        let value = sanitizeMessage(cleanInput[el] || '');
        if (el === 'title') {
          value = escapeRegExp(value);
        }
        cleanInput[el] = value;
      }
      queue[el] = cleanInput[el];
    });

    await queue.save();

    return queue;
  },
};

export default updateQueue;
