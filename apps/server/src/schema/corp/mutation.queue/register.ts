import { FieldConfig } from 'app/schema/graphql-compose';
import { dropSession } from 'app/schema/corp/context';
import { User, UserModel } from 'app/models/User';
import { QueryUserTC, UserITC } from 'app/schema/types';
import { escapeRegExp, escapePhoneRegExp } from 'app/utils/escapeRegExp';
import { sanitizeMessage } from 'app/utils/sanitizeMessage';

const register: FieldConfig<{ input: UserModel }> = {
  type: QueryUserTC,
  args: {
    input: UserITC.getTypeNonNull(),
  },
  resolve: async (_, args, context) => {
    await dropSession(context);
    const cleanInput: UserModel = JSON.parse(JSON.stringify(args.input));
    cleanInput['title'] = escapeRegExp(sanitizeMessage(cleanInput['title'] || ''));
    cleanInput['phone'] = escapePhoneRegExp(sanitizeMessage(cleanInput['phone'] || ''));
    const registerResult = await User.register(cleanInput);
    return registerResult;
  },
};

export default register;
