import { FieldConfig } from 'app/schema/graphql-compose';
import { QueryClientQueueTC } from 'app/schema/types';
import { Queue } from 'app/models/Queue';
import { checkQueuePaid } from 'app/utils/checkQueuePaid';

const queue: FieldConfig<{ link: string }> = {
  type: QueryClientQueueTC,
  args: {
    link: 'String!',
  },
  resolve: async (_, args) => {
    const queue = await Queue.findOne({ link: args.link })
      .populate('breakEntry')
      .populate('currentEntry')
      .populate('entries')
      .exec();

    if (!checkQueuePaid(queue)) {
      return null;
    }
    return queue;
  },
};

export default queue;
