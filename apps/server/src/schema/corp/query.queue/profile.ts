import { FieldConfig } from 'app/schema/graphql-compose';
import { QueryUserTC } from 'app/schema/types';
import { getFieldsForPath } from 'app/utils/getRelationsForField';
import { UserDocument } from 'app/models/User';

const profile: FieldConfig<{}> = {
  type: QueryUserTC,
  resolve: async (_, __, context, info) => {
    const user: UserDocument | undefined = context.req.user as any;
    if (!user) {
      return null;
    }

    const queryFields = getFieldsForPath(info);

    const needQueue = queryFields.find((el) => el.startsWith('queue'));
    const needQueueCurent = queryFields.find((el) => el.startsWith('queue.currentEntry'));
    const needQueueCurentUser = queryFields.find((el) => el.startsWith('queue.currentEntry.user'));
    const needQueueBreak = queryFields.find((el) => el.startsWith('queue.breakEntry'));
    const needQueueBreakUser = queryFields.find((el) => el.startsWith('queue.breakEntry.user'));
    const needQueueEntries = queryFields.find((el) => el.startsWith('queue.entries'));
    const needQueueEntriesUser = queryFields.find((el) => el.startsWith('queue.entries.user'));

    const needEntries = queryFields.find((el) => el.startsWith('entries'));
    const needEntriesQueue = queryFields.find((el) => el.startsWith('entries.queue'));

    if (needQueue && user.queue) {
      if (needQueueEntries || needQueueCurent || needQueueBreak) {
        const pupulateQueueFields: any[] = [];

        if (needQueueEntriesUser) {
          pupulateQueueFields.push({ path: 'entries', populate: { path: 'user' } });
        } else if (needQueueEntries) {
          pupulateQueueFields.push({ path: 'entries' });
        }

        if (needQueueCurentUser) {
          pupulateQueueFields.push({ path: 'currentEntry', populate: { path: 'user' } });
        } else if (needQueueCurent) {
          pupulateQueueFields.push({ path: 'currentEntry' });
        }

        if (needQueueBreakUser) {
          pupulateQueueFields.push({ path: 'breakEntry', populate: { path: 'user' } });
        } else if (needQueueBreak) {
          pupulateQueueFields.push({ path: 'breakEntry' });
        }

        user.populate({ path: 'queue', populate: pupulateQueueFields });
      } else {
        user.populate('queue');
      }
    }
    if (needEntries) {
      if (needEntriesQueue) {
        user.populate({ path: 'entries', populate: { path: 'queue' } });
      } else {
        user.populate('entries');
      }
    }

    if (needQueue || needEntries) {
      await user.execPopulate();
    }

    return user;
  },
};

export default profile;
