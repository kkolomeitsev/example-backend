import { FieldConfig } from 'app/schema/graphql-compose';

const testSes: FieldConfig<{ time: string }> = {
  type: 'String',
  resolve: async (_, __, context) => {
    if (!context.req.session?.userId) {
      return null;
    }
    return context.req.sessionID;
  },
};

export default testSes;
