import { FilterQuery } from 'mongoose';
import { composeWithMongoose, schemaComposer } from '../graphql-compose';
import { Queue, QueueDocument } from 'app/models/Queue';
import { QueueStatusEnumTC } from './QueueStatusEnumTC';
import {
  QueryEntryTC,
  QueryMasterEntryTC,
  QueryClientEntryTC,
  QueryClientEntryWithTimeTC,
  QueryClientMyEntryTC,
} from './Entry';
import moment from 'moment';
import { Entry, EntryDocument } from 'app/models/Entry';
import { UserDocument } from 'app/models/User';

export const QueueTC = composeWithMongoose(Queue as any);

/**
 * Для владельца
 */

export const QueryQueueTC = QueueTC.clone('UserQueueData');

QueryQueueTC.removeField(['owner', 'entries', 'breakEntry', 'currentEntry', 'status']).addFields({
  status: QueueStatusEnumTC,
  breakEntry: () => QueryMasterEntryTC,
  currentEntry: () => QueryMasterEntryTC,
  entries: () => [QueryMasterEntryTC],
  totalEntries: {
    type: () => [QueryMasterEntryTC],
    resolve: async (source: QueueDocument) => {
      const currentDate = moment().utcOffset(source.gmt);

      const findConditions: FilterQuery<EntryDocument> = {
        queue: source._id,
        date: {
          $gte: currentDate.clone().startOf('day').toDate(),
          $lte: currentDate.clone().endOf('day').toDate(),
        },
        active: true,
      };

      if (source.currentEntry) {
        findConditions._id = { $ne: source.currentEntry as any };
      }

      const todayEntries = await Entry.find(findConditions)
        .sort('commonTurn')
        .populate('user')
        .exec();

      return todayEntries;
    },
  },
});

export const QueueITC = QueueTC.getITC();

QueueITC.removeField([
  'owner',
  'entries',
  'createdAt',
  'updatedAt',
  '_id',
  'status',
  'paidFor',
  'link',
  'breakEntry',
  'currentEntry',
  'gmt',
  'commonLimit',
  'dayLimit',
  'bookingPeriodDays',
  'averageServiceMinutes',
])
  .addFields({
    gmt: 'Int',
    commonLimit: 'Int',
    dayLimit: 'Int',
    bookingPeriodDays: 'Int',
    averageServiceMinutes: 'Int',
  })
  .makeFieldNonNull(['title']);

export const UpdateQueueITC = QueryQueueTC.getITC()
  .clone('UpdateUserQueueInput')
  .removeField([
    'entries',
    'createdAt',
    'updatedAt',
    '_id',
    'status',
    'paidFor',
    'link',
    'breakEntry',
    'currentEntry',
    'gmt',
    'commonLimit',
    'dayLimit',
    'bookingPeriodDays',
    'averageServiceMinutes',
    'totalEntries',
  ])
  .addFields({
    gmt: 'Int',
    commonLimit: 'Int',
    dayLimit: 'Int',
    bookingPeriodDays: 'Int',
    averageServiceMinutes: 'Int',
  });

export const QueueStatusChangeITC = schemaComposer
  .createInputTC('QueueStatusChangeInput')
  .addFields({
    breakMessage: 'String',
    breakEntry: 'FilterEntryInput',
    status: QueueStatusEnumTC,
  });

/**
 * Для клиента
 */

export const QueryClientQueueTC = QueryQueueTC.clone('ClientQueueData')
  .removeField([
    'entries',
    'createdAt',
    'updatedAt',
    'breakEntry',
    'currentEntry',
    'totalEntries',
    'paidFor',
  ])
  .addFields({
    breakEntry: () => QueryClientEntryTC,
    currentEntry: () => QueryClientEntryTC,
    entries: () => [QueryClientEntryWithTimeTC],
    myEntries: {
      type: () => [QueryClientMyEntryTC],
      resolve: async (source, _, context) => {
        const user: UserDocument | undefined = context.req.user as any;
        if (!user) {
          return null;
        }

        const findConditions: FilterQuery<EntryDocument> = {
          queue: source._id,
          user: user._id,
          active: true,
        };

        const myEntries = await Entry.find(findConditions).sort('date').exec();

        if (myEntries.length) {
          const currentDate = moment().utcOffset(source.gmt);

          myEntries.forEach((el) => {
            const entryDate = moment(el.date).utcOffset(source.gmt);
            const daysDiff = entryDate
              .clone()
              .startOf('day')
              .diff(currentDate.clone().startOf('day'), 'days');
            if (daysDiff === 0) {
              (el as any).isToday = true;
            }
          });
        }

        return myEntries;
      },
    },
  });

QueryEntryTC.addFields({
  queue: () => QueryClientQueueTC,
});

export const FilterQueueITC = schemaComposer.createInputTC('FilterQueueInput').addFields({
  _id: 'MongoID',
  link: 'String',
});

/**
 * Для мастера
 */
