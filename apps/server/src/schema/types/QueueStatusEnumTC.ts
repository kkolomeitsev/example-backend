import { schemaComposer } from 'app/schema/graphql-compose';
import { QueueStatus } from 'app/models/Queue';

export const QueueStatusEnumTC = schemaComposer.createEnumTC({
  name: 'QueueStatus',
  values: {
    OPEN: { value: QueueStatus.OPEN },
    CLOSED: { value: QueueStatus.CLOSED },
    BREAK: { value: QueueStatus.BREAK },
  },
});
