import { composeWithMongoose } from '../graphql-compose';
import { User } from 'app/models/User';
import { QueryQueueTC } from './Queue';
import { QueryEntryTC, QueryMasterEntryTC } from './Entry';

export const UserTC = composeWithMongoose(User as any);

/**
 * Для владельца
 */

export const QueryUserTC = UserTC.clone('UserProfileData');

QueryUserTC.removeField(['hashedPassword', 'active', 'username', 'entries', 'queue']).addFields({
  username: 'EmailAddress!',
  queue: () => QueryQueueTC,
  entries: () => [QueryEntryTC],
});

export const UserITC = UserTC.getITC();

UserITC.removeField([
  'hashedPassword',
  'active',
  'createdAt',
  'updatedAt',
  '_id',
  'username',
  'entries',
  'queue',
])
  .addFields({
    password: 'String!',
    username: 'EmailAddress!',
  })
  .makeFieldNonNull(['phone', 'title']);

export const UpdateUserITC = QueryUserTC.getITC()
  .clone('UpdateUserProfileInput')
  .removeField(['entries', 'queue', 'username', 'createdAt', 'updatedAt', '_id']);

/**
 * Для клиента
 */

/**
 * Для мастера
 */

export const QueryMasterUserTC = QueryUserTC.clone('MasterUserData').removeField([
  'queue',
  'entries',
  'createdAt',
  'updatedAt',
]);

QueryMasterEntryTC.addFields({
  user: QueryMasterUserTC,
});
