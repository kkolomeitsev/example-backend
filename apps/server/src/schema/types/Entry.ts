import { composeWithMongoose, schemaComposer } from '../graphql-compose';
import { Entry } from 'app/models/Entry';

export const EntryTC = composeWithMongoose(Entry as any);

/**
 * Для владельца
 */

export const QueryEntryTC = EntryTC.clone('UserEntryData');

QueryEntryTC.removeField(['user', 'queue']);

export const EntryITC = EntryTC.getITC();

EntryITC.removeField(['user', 'queue', 'createdAt', 'updatedAt', '_id', 'active', 'commonTurn'])
  .addFields({
    queue: () => 'FilterQueueInput!',
  })
  .makeFieldNonNull(['date']);

export const UpdateEntryITC = QueryEntryTC.getITC()
  .clone('UpdateUserEntryInput')
  .removeField(['queue', 'createdAt', 'updatedAt', '_id', 'active', 'commonTurn']);

/**
 * Для клиента
 */

export const QueryClientEntryTC = schemaComposer.createObjectTC('ClientEntryData').addFields({
  _id: 'MongoID',
  commonTurn: 'Int',
});

export const QueryClientEntryWithTimeTC = QueryClientEntryTC.clone(
  'ClientEntryWithTimeData'
).addFields({
  date: 'Date',
});

export const QueryClientMyEntryTC = QueryClientEntryWithTimeTC.clone('ClientMyEntryData').addFields(
  {
    isToday: 'Boolean',
  }
);

export const FilterEntryITC = schemaComposer.createInputTC('FilterEntryInput').addFields({
  _id: 'MongoID',
});

/**
 * Для мастера
 */

export const QueryMasterEntryTC = QueryEntryTC.clone('MasterEntryData').removeField([
  'queue',
  'createdAt',
  'updatedAt',
  'active',
]);
