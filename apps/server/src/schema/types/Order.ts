import { composeWithMongoose } from '../graphql-compose';
import { Order } from 'app/models/Order';

export const OrderTC = composeWithMongoose(Order as any);

/**
 * Для владельца
 */

export const QueryOrderTC = OrderTC.clone('UserOrderData');

QueryOrderTC.removeField(['user', 'queue']).addFields({
  queue: () => 'UserQueueData!',
});

export const OrderITC = OrderTC.getITC();

OrderITC.removeField([
  'user',
  'queue',
  'createdAt',
  'updatedAt',
  '_id',
  'status',
  'amount',
]).addFields({
  amount: 'Int!',
});
