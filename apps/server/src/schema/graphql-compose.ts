import { Schema, Model, Document } from 'mongoose';
import { GraphQLResolveInfo } from 'graphql';
import {
  SchemaComposer,
  ObjectTypeComposer,
  ObjectTypeComposerFieldConfigAsObjectDefinition,
} from 'graphql-compose';
import {
  composeWithMongoose as _composeWithMongoose,
  composeWithMongooseDiscriminators as _composeWithMongooseDiscriminators,
  convertSchemaToGraphQL as _convertSchemaToGraphQL,
  DiscriminatorTypeComposer,
  ComposeWithMongooseOpts,
  ComposeWithMongooseDiscriminatorsOpts,
} from 'graphql-compose-mongoose';
import { TContext } from './corp/context';
import { Typegoose } from '@typegoose/typegoose';

export { TContext };
export const schemaComposer: SchemaComposer<TContext> = new SchemaComposer();
export type GraphQLFieldResolver<TArgs, Payload> = (
  source: any,
  args: TArgs,
  context: TContext,
  info: GraphQLResolveInfo
) => Promise<Payload>;

export function composeWithMongoose<TDoc extends Typegoose = any>(
  model: Model<TDoc & Document>,
  opts?: ComposeWithMongooseOpts<TContext>
): ObjectTypeComposer<TDoc & Document, TContext> {
  return _composeWithMongoose(model, { schemaComposer, ...opts });
}
export function composeWithMongooseDiscriminators<TDoc extends Document = any>(
  model: Model<TDoc>,
  opts?: ComposeWithMongooseDiscriminatorsOpts<TContext>
): DiscriminatorTypeComposer<TDoc & Document, TContext> {
  return _composeWithMongooseDiscriminators(model, { schemaComposer, ...opts });
}

export function convertSchemaToGraphQL(
  ms: Schema<any>,
  typeName: string
): ObjectTypeComposer<any, TContext> {
  return _convertSchemaToGraphQL(ms, typeName, schemaComposer);
}

export type FieldConfig<TArgs> = ObjectTypeComposerFieldConfigAsObjectDefinition<
  any,
  TContext,
  TArgs
>;

export {
  upperFirst,
  graphql,
  GraphQLJSON,
  ObjectTypeComposerFieldConfigAsObjectDefinition,
  ObjectTypeComposerAsObjectDefinition,
  ObjectTypeComposerFieldConfig,
  ObjectTypeComposerArgumentConfigMapDefinition,
  ResolverRpCb,
  ResolverResolveParams,
  Extensions,
} from 'graphql-compose';
export { GraphQLMongoID } from 'graphql-compose-mongoose';
export { GraphQLNonNull } from 'graphql';
