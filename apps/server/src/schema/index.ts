import { ApolloServer } from 'apollo-server-express';
import cookieParser from 'cookie-parser';
import { RequestHandler } from 'express';
import { initSessionRequestHandler } from 'app/boot';
import './commonTypes';
import './types';

export const schema = require('./corp').buildSchema();

export const prepareContext = require('./corp').prepareContext;

const cookieParserRH = cookieParser();
const sessionRH = initSessionRequestHandler();

export const server = new ApolloServer({
  schema,
  context: prepareContext,
  introspection: true,
  playground: {
    settings: {
      'request.credentials': 'include',
    },
  } as any,
  formatError: (error) => {
    return error;
  },
  subscriptions: {
    onConnect: async function prepareWebsocketContext(params: any, ws: any, context: any) {
      const req = context?.request;

      if (!req) {
        throw new Error(
          'Cannot recieve Request from Websocket. Some breaking changes in SubscriptionServer?'
        );
      }

      await promisifyExpressMiddleware(cookieParserRH, req);

      await promisifyExpressMiddleware(sessionRH, req);

      return prepareContext({ req });
    },
  },
});

function promisifyExpressMiddleware(rh: RequestHandler, req: any): Promise<any> {
  return new Promise((resolve, reject) => {
    // в вебслокетах нет респонса
    // поэтому эмулируем его для миддлвар
    const rejectResponse = (data: any) => reject(new Error(data));
    const resMock = {
      setHeader: () => undefined,
      end: rejectResponse,
      send: rejectResponse,
      status: rejectResponse,
      json: rejectResponse,
    } as any;

    try {
      rh(req || {}, resMock, (e: any) => {
        if (e) reject(e);
        resolve();
      });
    } catch (e) {
      reject(e);
    }
  });
}
