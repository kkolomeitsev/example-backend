import EnvValue from './decorators/EnvValue';

export default class ENV {
  @EnvValue()
  public static ENV_TYPE: string;

  @EnvValue()
  public static CORS_DOMAIN: string;

  @EnvValue()
  public static MONGODB_URI: string;

  @EnvValue()
  public static MONGODB_SESSIONS_URI: string;

  @EnvValue()
  public static POSTGRES_HOST: string;

  @EnvValue()
  public static POSTGRES_USERNAME: string;

  @EnvValue()
  public static POSTGRES_PASSWORD: string;

  @EnvValue()
  public static POSTGRES_DATABASE: string;

  @EnvValue()
  public static POSTGRES_PORT: number;

  @EnvValue()
  public static POSTGRES_SYNCHRONIZE: boolean;

  @EnvValue()
  public static POSTGRES_LOGGING: boolean;

  @EnvValue()
  public static BA_NAME: string;

  @EnvValue()
  public static BA_P: string;

  @EnvValue()
  public static HMAC_S: string;

  @EnvValue()
  public static N_IPS: string;

  @EnvValue()
  public static PORT = 4000;
}
