export default function EnvValue(name: string | null = null) {
  return (target: any, propertyKey: string | symbol) => {
    const propertyType = Reflect.getMetadata('design:type', target, propertyKey);
    const k = name || (propertyKey as string);
    const v = process.env[k];
    let value;

    if (v) {
      switch (propertyType) {
        case Number:
          value = parseFloat(v);
          break;
        case Boolean:
          const t = v.toLowerCase();
          value = t === 'true' || t === '1' || t === 'on' || t === 'yes';
          break;
        default:
          value = v;
      }
    } else {
      value = target[k];
    }
    Object.defineProperty(target, propertyKey, {
      value,
      writable: false,
      enumerable: true,
    });
  };
}
