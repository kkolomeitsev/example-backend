import { Application } from 'express';
import helmet from 'helmet';
import mongoose from 'mongoose';
import bodyParser from 'body-parser';
import cookieParser from 'cookie-parser';
import session from 'express-session';
import connectMongo from 'connect-mongo';
import passport from 'passport';
import { Strategy as LocalStrategy } from 'passport-local';
import { ENV } from 'app/configs';
import sendHttpError from 'app/middlewares/sendHttpError';
import { User, UserDocument } from 'app/models/User';
import { connect } from 'app/utils/db';
import { HttpError } from 'app/utils/errors';
import { data } from 'app/utils/dataMap';

export function initSessionRequestHandler() {
  const environmentType = ENV.ENV_TYPE === 'production' ? 'production' : 'development';

  if (!data.sessionConnection) {
    data.sessionConnection = mongoose.createConnection(ENV.MONGODB_SESSIONS_URI, {
      useCreateIndex: true,
      useNewUrlParser: true,
      useUnifiedTopology: true,
    });
  }
  const sessionConnection = data.sessionConnection;

  const connectMongoStore = connectMongo(session);

  if (!data.mongoStore) {
    data.mongoStore = new connectMongoStore({
      mongooseConnection: sessionConnection,
      collection: 'sessions',
      stringify: false,
      serialize: (session: any) => {
        const obj: any = {};
        let prop;

        for (prop in session) {
          if (prop === 'cookie') {
            obj.cookie = session.cookie.toJSON ? session.cookie.toJSON() : session.cookie;
          } else {
            obj[prop] = session[prop];
          }
        }

        return obj;
      },
      unserialize: (x: any) => x,
    } as any);
  }

  return session({
    secret: 'secret',
    resave: false,
    saveUninitialized: true,
    name: 'x_sid',
    cookie: {
      path: '/',
      httpOnly: true,
      secure: environmentType === 'production' ? true : false,
      domain: `.${ENV.CORS_DOMAIN}`,
    },
    store: data.mongoStore,
  });
}

export default async (app: Application) => {
  app.set('trust proxy', 1);

  await mongoose.connect(ENV.MONGODB_URI, {
    useCreateIndex: true,
    useNewUrlParser: true,
    useUnifiedTopology: true,
  });

  const sessionRH = initSessionRequestHandler();

  const dbConnection = await connect();
  if (!dbConnection) throw new Error('Error can not connect to database!');

  app.use(helmet());

  app.use(sendHttpError);

  app.use(bodyParser.urlencoded({ extended: false }));

  app.use(bodyParser.json());

  app.use(cookieParser());

  app.use((req, res, next) => sessionRH(req, res, next));

  app.use(passport.initialize());

  app.use(passport.session());

  passport.use(
    'local',
    new LocalStrategy((username, password, done) => {
      User.authorize(username, password)
        .then((user) => done(null, user))
        .catch((err) => done(err));
    })
  );

  passport.serializeUser((user: UserDocument, done) => {
    if (!user) {
      return done(new HttpError(403, 'CHECK USERNAME AND PSWD'));
    }

    done(null, user._id);
  });

  passport.deserializeUser((userId: string, done) => {
    User.findById(userId, (err, user) => {
      if (!user) {
        done(new HttpError(403, 'WRONG LOGIN OR PSWD'));
      } else {
        done(err, user);
      }
    });
  });
};
