/* eslint-disable @typescript-eslint/camelcase */
/* eslint-disable @typescript-eslint/no-non-null-assertion */
import bcrypt from 'bcryptjs';
import passport from 'passport';
import {
  prop,
  arrayProp,
  index,
  DocumentType,
  ReturnModelType,
  getModelForClass,
  Ref,
} from '@typegoose/typegoose';
import { QueueModel } from 'app/models/Queue';
import { EntryModel } from 'app/models/Entry';
import { HttpError } from 'app/utils/errors';
import { ResolverConfig } from 'app/typings/commonTypes';

function setRememberCookie(session: any) {
  session.cookie.expires = new Date(Date.now() + 30 * 24 * 60 * 60 * 1000);
  session.cookie.maxAge = 30 * 24 * 60 * 60 * 1000;
}

const saltRounds = 10;

@index({ username: 1 }, { unique: true })
export class UserModel {
  _plainPassword: string;

  @prop({ trim: true, lowercase: true, required: true })
  public username: string;

  @prop({ default: true })
  public active: boolean;

  @prop({ trim: true, required: true })
  public title: string;

  @prop({ trim: true, required: true })
  public phone: string;

  @prop({ ref: 'QueueModel' })
  public queue?: Ref<QueueModel>;

  @arrayProp({ ref: 'EntryModel', default: [] })
  public entries?: Ref<EntryModel>[];

  @prop()
  public hashedPassword: string;

  get password() {
    return this._plainPassword;
  }

  set password(this: DocumentType<UserModel>, password: string) {
    this._plainPassword = password;
    this.hashedPassword = this.encryptPassword(password);
  }

  public encryptPassword(this: DocumentType<UserModel>, password: string) {
    const hashedPass = bcrypt.hashSync(password, saltRounds);
    return hashedPass;
  }

  public async checkPassword(this: DocumentType<UserModel>, password: string) {
    const match = await bcrypt.compare(password, this.hashedPassword);

    return match;
  }

  public static async authorize(
    this: ReturnModelType<typeof UserModel>,
    username: string,
    password: string
  ) {
    if (!(username && password)) {
      throw new HttpError(403, 'WRONG LOGIN OR PSWD');
    }

    const thisUser = await this.findOne({ username, active: true }).exec();
    let match = false;

    if (thisUser) {
      match = await thisUser.checkPassword(password);
    } else {
      throw new HttpError(403, 'USER NOT FOUND');
    }

    if (match) {
      return thisUser;
    } else {
      throw new HttpError(403, 'WRONG LOGIN OR PSWD');
    }
  }

  public static async login(this: ReturnModelType<typeof UserModel>, config: ResolverConfig) {
    const { args, context } = config;
    if (context.req.session?.userId) {
      throw new HttpError(403, 'Already authorized');
    }
    const auth = new Promise((resolve, reject) =>
      passport.authenticate('local', (err, user: DocumentType<UserModel>) => {
        if (err instanceof HttpError && err.message === 'USER NOT FOUND') {
          return resolve({ notFound: true });
        }
        if (err) {
          return reject(err);
        }
        context.req.logIn(user, (errLogin: any) => {
          if (errLogin) {
            return reject(errLogin);
          }
          const result: any = { authorize: true };
          if (context.req.session) {
            if (args.remember) {
              setRememberCookie(context.req.session);
            }
            context.req.session.userId = user.username;
            context.req.session.save((err) => {
              if (err) {
                return reject(err);
              } else {
                return resolve(result);
              }
            });
          } else {
            return reject(result);
          }
        });
      })({ body: { username: args.username, password: args.password } }, context.res)
    );

    const isAuth = await auth;
    return isAuth;
  }

  public static async register(
    this: ReturnModelType<typeof UserModel>,
    input: { username: string; title: string; phone: string; password: string }
  ) {
    const newUser = new this(input);

    const result = await newUser.save();

    return result;
  }

  public async updateData(
    this: DocumentType<UserModel>,
    input: { [P in keyof UserModel]: UserModel[P] }
  ) {
    for (const key in input) {
      this[key] = input[key];
    }
    await this.save();
    const result = this.toObject();
    return result;
  }
}

export const User = getModelForClass(UserModel, {
  schemaOptions: {
    collection: 'users',
    timestamps: true,
  },
});

export type UserDocument = DocumentType<UserModel>;
