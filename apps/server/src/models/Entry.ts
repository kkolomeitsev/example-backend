/* eslint-disable @typescript-eslint/camelcase */
import { prop, DocumentType, getModelForClass, Ref, index } from '@typegoose/typegoose';
import { UserModel } from 'app/models/User';
import { QueueModel } from 'app/models/Queue';

@index({ active: 1, queue: 1, date: 1, commonTurn: 1 }, { unique: true })
export class EntryModel {
  @prop({ required: true })
  public date: Date;

  @prop({ required: true, min: 1 })
  public commonTurn: number;

  @prop({ trim: true, default: '' })
  public description: string;

  @prop({ default: true })
  public active: boolean;

  @prop({ default: false })
  public inLife: boolean;

  @prop({ ref: 'UserModel', required: true })
  public user?: Ref<UserModel>;

  @prop({ ref: 'QueueModel', required: true })
  public queue?: Ref<QueueModel>;
}

export const Entry = getModelForClass(EntryModel, {
  schemaOptions: {
    collection: 'entries',
    timestamps: true,
  },
});

export type EntryDocument = DocumentType<EntryModel>;
