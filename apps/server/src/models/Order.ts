/* eslint-disable @typescript-eslint/camelcase */
import { prop, DocumentType, getModelForClass, Ref, index } from '@typegoose/typegoose';
import { UserModel } from 'app/models/User';
import { QueueModel } from 'app/models/Queue';

@index({ status: 1, user: 1, queue: 1 })
export class OrderModel {
  @prop({ default: 'pending' })
  public status: string;

  @prop({ required: true })
  public amount: string;

  @prop({ ref: 'UserModel', required: true })
  public user?: Ref<UserModel>;

  @prop({ ref: 'QueueModel', required: true })
  public queue?: Ref<QueueModel>;
}

export const Order = getModelForClass(OrderModel, {
  schemaOptions: {
    collection: 'orders',
    timestamps: true,
  },
});

export type OrderDocument = DocumentType<OrderModel>;
