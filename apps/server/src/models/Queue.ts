/* eslint-disable @typescript-eslint/camelcase */
import { prop, arrayProp, index, DocumentType, getModelForClass, Ref } from '@typegoose/typegoose';
import { UserModel } from 'app/models/User';
import { EntryModel } from 'app/models/Entry';

export enum QueueStatus {
  OPEN = 'open',
  CLOSED = 'closed',
  BREAK = 'break',
}

@index({ owner: 1, title: 1 }, { unique: true })
export class QueueModel {
  @prop({ trim: true, required: true })
  public title: string;

  @prop({ trim: true, required: true, unique: true })
  public link: string;

  @prop({ trim: true, default: '' })
  public description: string;

  @prop({ default: 6 })
  public gmt: number;

  @arrayProp({ items: String, default: ['00:00:00.000', '23:59:59.999'] })
  public workingHours: string[];

  @arrayProp({ items: String, default: [] })
  public recurringWeekend: string[];

  @arrayProp({ items: String, default: [] })
  public holidays: string[];

  @prop({ default: 0, min: 0 })
  public commonLimit: number;

  @prop({ default: 0, min: 0 })
  public dayLimit: number;

  @prop({ default: false })
  public limitLifeQueue: boolean;

  @prop({ default: 1, min: 0 })
  public bookingPeriodDays: number;

  @prop({ default: 20, min: 1 })
  public averageServiceMinutes: number;

  @prop({ enum: QueueStatus, default: QueueStatus.CLOSED })
  public status: QueueStatus;

  @prop({ trim: true, default: '' })
  public breakMessage: string;

  @prop()
  public paidFor: Date;

  @prop({ ref: 'EntryModel' })
  public breakEntry?: Ref<EntryModel>;

  @prop({ ref: 'UserModel', required: true })
  public owner?: Ref<UserModel>;

  @prop({ ref: 'EntryModel' })
  public currentEntry?: Ref<EntryModel>;

  @arrayProp({ ref: 'EntryModel', default: [] })
  public entries?: Ref<EntryModel>[];
}

export const Queue = getModelForClass(QueueModel, {
  schemaOptions: {
    collection: 'queues',
    timestamps: true,
  },
});

export type QueueDocument = DocumentType<QueueModel>;
