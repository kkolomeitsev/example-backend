// Добавляем возможность отправить ошибку в response

import express from 'express';
import { HttpError } from 'app/utils/errors';

export default function(req: express.Request, res: express.Response, next: express.NextFunction) {
  res.sendHttpError = function(error: HttpError) {
    res.status(error.status);
    if (req.headers['x-requested-with'] === 'XMLHttpRequest') {
      res.json(error.message);
    } else {
      res.send(`Error: ${error}`);
    }
  };
  next();
}
