import { TContext } from 'app/schema/corp/context';

export interface ResolverConfig {
  source: any;
  args: any;
  context: TContext;
  info: any;
}
