import { Decimal } from 'decimal.js';

export type Decimal128 = Decimal;
export const Decimal128 = Decimal.clone({ precision: 40, toExpNeg: -34, toExpPos: 34 });
