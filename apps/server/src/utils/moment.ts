import libMoment, { MomentInput, MomentFormatSpecification, Moment } from 'moment';
export { Moment } from 'moment';

export function moment(
  _inp?: MomentInput,
  _format?: MomentFormatSpecification,
  _strict?: boolean
): Moment {
  // eslint-disable-next-line prefer-rest-params
  return libMoment(...arguments).utcOffset(0);
}
