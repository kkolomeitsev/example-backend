import { GraphQLResolveInfo } from 'graphql';
import { getProjectionFromAST, ProjectionType } from 'graphql-compose/lib/utils/projection';

function fillFlatProjection(
  projection: ProjectionType,
  target: any,
  allSimpleFields: string[] = [],
  allComplexFields: string[] = [],
  prefix = ''
) {
  target['simpleFields'] = [];
  target['simpleFieldsPath'] = [];
  target['complexFields'] = {};
  target['complexFieldsPath'] = [];

  Object.keys(projection).forEach((el) => {
    const newPrefix = prefix ? `${prefix}.${el}` : el;

    if (Object.keys(projection[el]).length) {
      target.complexFieldsPath.push(newPrefix);
      allComplexFields.push(newPrefix);
      target.complexFields[el] = {};
      fillFlatProjection(
        projection[el],
        target.complexFields[el],
        allSimpleFields,
        allComplexFields,
        newPrefix
      );
    } else {
      target.simpleFields.push(el);
      target.simpleFieldsPath.push(newPrefix);
      allSimpleFields.push(newPrefix);
    }
  });
}

function getFlatProjection(projection: ProjectionType) {
  const flatProjection = {};
  const allSimpleFields: string[] = [];
  const allComplexFields: string[] = [];

  fillFlatProjection(projection, flatProjection, allSimpleFields, allComplexFields);

  return {
    flatProjection,
    allSimpleFields,
    allComplexFields,
  };
}

export function getRelationsForField(info: GraphQLResolveInfo, fieldPath = ''): string[] {
  const astProjection = getProjectionFromAST(info);

  const flatProjection = getFlatProjection(astProjection);
  const allComplexFields: string[] = flatProjection.allComplexFields;

  const relations = fieldPath
    ? allComplexFields
        .filter((el) => el.startsWith(`${fieldPath}.`))
        .map((el) => el.substr(fieldPath.length - el.length + 1))
    : allComplexFields;

  return relations;
}

export function getFieldsForPath(info: GraphQLResolveInfo, fieldPath = ''): string[] {
  const astProjection = getProjectionFromAST(info);

  const flatProjection = getFlatProjection(astProjection);
  const allSimpleFields: string[] = flatProjection.allSimpleFields;

  const fields = fieldPath
    ? allSimpleFields
        .filter((el) => el.startsWith(`${fieldPath}.`))
        .map((el) => el.substr(fieldPath.length - el.length + 1))
    : allSimpleFields;

  return fields;
}
