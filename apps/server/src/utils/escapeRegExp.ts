export function escapeRegExp(str: string) {
  return str.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, '\\$&');
}

export function escapePhoneRegExp(str: string) {
  return str.replace(/[\[\]\/\{\}\*\?\.\\\^\$\|]/g, '\\$&');
}
