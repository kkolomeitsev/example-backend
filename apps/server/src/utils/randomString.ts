/* eslint-disable no-plusplus */
import crypto from 'crypto';
import { Model } from 'mongoose';

export function randomString(length: number) {
  const chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';

  const randomBytes = crypto.randomBytes(length);
  const result = new Array(length);

  let cursor = 0;
  for (let i = 0; i < length; i++) {
    cursor += randomBytes[i];
    result[i] = chars[cursor % chars.length];
  }

  return result.join('');
}

export async function getUniqueRandomString(model: typeof Model, field: string): Promise<string> {
  const random = randomString(12);
  const findOptions: any = {};
  findOptions[field] = random;
  const notUnique = await model.findOne(findOptions).exec();
  if (!notUnique) {
    return random;
  } else {
    return getUniqueRandomString(model, field);
  }
}
