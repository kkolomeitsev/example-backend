export const sendMail = {};
// import nodemailer, { TransportOptions, SendMailOptions } from 'nodemailer';
// import { sanitizeMessage } from 'app/utils/sanitizeMessage';
// import { UserDocument } from 'app/models/User';
// import { ENV } from 'app/configs';

// function numberWithSpaces(x: number): string {
//   const parts = x.toString().split('.');
//   parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ' ');
//   return parts.join('.');
// }

// const SELF_EMAIL = '"Habl - вакансии, резюме" <admin@habl.kz>';

// const sendOptions = {
//   host: ENV.SENDMAIL_HOST,
//   port: ENV.SENDMAIL_PORT,
//   secure: ENV.SENDMAIL_SECURE,
//   auth: {
//     user: ENV.SENDMAIL_USER,
//     pass: ENV.SENDMAIL_PWD,
//   },
// };

// const transporter = nodemailer.createTransport(sendOptions as TransportOptions);

// const mailOptions: SendMailOptions = {
//   from: SELF_EMAIL,
//   to: '',
//   subject: '',
//   html: '',
// };

// async function send(mailOptions: SendMailOptions) {
//   const result = await new Promise((resolve, reject) => {
//     transporter.sendMail(mailOptions, (error, info) => {
//       if (error) {
//         return reject(error);
//       }
//       return resolve(`${info.messageId}`);
//     });
//   });

//   return result;
// }

// export const sendMail = async function (
//   to: string,
//   reason: string,
//   _options: { [key: string]: any } = {}
// ) {
//   mailOptions.to = to;
//   mailOptions.subject = reason;
//   mailOptions.html = `Тест письмо ололо`;

//   const result = await send(mailOptions);

//   return result;
// };

// export const mailToSupport = async function(from: string, message: string) {
//   const cleanMessage = sanitizeMessage(message);

//   const mailOptions: SendMailOptions = {
//     from: '"Habl - запрос в поддержку" <admin@habl.kz>',
//     to: SELF_EMAIL,
//     subject: 'Запрос в поддержку',
//     html: `
//       <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
//       <html>
//         <body>
//           <table border="0" cellpadding="0" cellspacing="0" style="margin:0;padding:0;border-collapse:collapse" width="100%">
//             <tr>
//               <td>
//                 <center style="max-width:600px;width:100%;">
//                   <table border="1" bordercolor="#cccccc" cellpadding="0" cellspacing="0" style="margin:0;padding:0;border-collapse:collapse" width="100%">
//                     <tr>
//                       <td style="padding:10px">От кого:</td>
//                       <td style="padding:10px">${from}</td>
//                     </tr>
//                     <tr>
//                       <td style="padding:10px">Запрос:</td>
//                       <td style="padding:10px">${cleanMessage}</td>
//                     </tr>
//                   </table>
//                 </center>
//               </td>
//             </tr>
//           </table>
//         </body>
//       </html>
//     `,
//   };

//   const result = await send(mailOptions);

//   return result;
// };
