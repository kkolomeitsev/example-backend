import sanitizeHtml from 'sanitize-html';

export function sanitizeMessage(message: string) {
  const cleanMessage = sanitizeHtml(message, {
    allowedTags: [
      'h3',
      'h4',
      'h5',
      'h6',
      'blockquote',
      'p',
      'ul',
      'ol',
      'li',
      'b',
      'i',
      'strong',
      'em',
      'hr',
      'br',
      'div',
    ],
    allowedAttributes: {
      '*': ['class', 'data-gramm', 'contenteditable'],
    },
    allowedIframeHostnames: [],
    selfClosing: ['br', 'hr'],
  });

  return cleanMessage;
}
