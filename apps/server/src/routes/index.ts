/* eslint-disable @typescript-eslint/no-non-null-assertion */
import express from 'express';
import crypto from 'crypto';
import auth from 'basic-auth';
import getGraphQLRouter from 'app/routes/graphql';
import { ENV } from 'app/configs';
import { Order } from 'app/models/Order';
import { Queue } from 'app/models/Queue';
import moment from 'moment';

export default function getAppRouter() {
  const router = express.Router();

  router.use('/notice/*', (req, res, next) => {
    const credentials = auth(req);

    const name = ENV.BA_NAME || 'cpnoticeuser';
    const pass = ENV.BA_P || 'dOtitfSwRMc4HFti1WuosFB37';

    if (!credentials || credentials.name !== name || credentials.pass !== pass) {
      res.status(401);
      res.setHeader('WWW-Authenticate', 'Basic realm="Access to notify"');
      res.end('Access denied');
      return;
    }

    const hmacSecret = ENV.HMAC_S || 'd4b1f276ecb95404ae57c12787842c6e';

    const checkHeader = req.get('X-Content-HMAC') || '';

    const hmac = crypto.createHmac('sha256', hmacSecret);

    const bodyString = req.body
      ? Object.keys(req.body)
          .map((el) => `${el}=${req.body[el]}`)
          .join('&')
      : 'body';

    hmac.update(bodyString);

    const hmacDecoded = hmac.digest('base64');

    if (hmacDecoded.trim() !== checkHeader.trim()) {
      res.status(401);
      res.end('Check failed');
      return;
    }

    return next();
  });

  router.post('/notice/pay', (req, res, next) => {
    Order.findById(req.body['InvoiceId'], (err, result) => {
      if (err) {
        return next(err);
      }
      if (!result) {
        return next(new Error(`Wrong order ID: ${req.body['InvoiceId']}`));
      }

      result.status = 'completed';

      result.save((err) => {
        if (err) {
          return next(err);
        }

        Queue.findById(result.queue, (err, queue) => {
          if (err) {
            return next(err);
          }

          if (!queue) {
            return next(new Error(`Wrong queue ID: ${result.queue}`));
          }

          const currentDate = moment().utcOffset(queue.gmt).endOf('day');

          queue.paidFor = currentDate.add(1, 'M').toDate();

          queue.save((err) => {
            if (err) {
              return next(err);
            }

            new Order({ user: result.user, queue: result.queue, amount: '4990' }).save((err) => {
              if (err) {
                return next(err);
              }

              res.json({ status: 'success' });
            });
          });
        });
      });
    });
  });

  router.use(getGraphQLRouter());

  return router;
}
