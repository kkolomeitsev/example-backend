/* eslint-disable @typescript-eslint/camelcase */
import express from 'express';
import { server } from 'app/schema';
import { ENV } from 'app/configs';

export default function getGraphQLRouter() {
  const router = express.Router();

  const whitelist =
    ENV.ENV_TYPE === 'production'
      ? []
      : [
          'http://localhost:3000',
          'http://localhost:3001',
          'http://localhost:4000',
          'http://localhost:4001',
        ];
  server.applyMiddleware({
    app: router as any,
    path: '/graphql',
    cors: {
      credentials: true,
      allowedHeaders: ['Content-Type', 'Cookie', 'Origin', 'X-Requested-With', 'Accept'],
      methods: ['GET', 'HEAD', 'POST', 'OPTIONS'],
      origin: (origin: string | undefined, callback: any) => {
        if (typeof origin === 'undefined') {
          callback(null);
        } else if (typeof origin === 'string' && origin.toLowerCase().endsWith(ENV.CORS_DOMAIN)) {
          callback(null, true);
        } else if (whitelist.indexOf(origin) !== -1) {
          callback(null, true);
        } else {
          callback(null);
        }
      },
    },
  });

  return router;
}
